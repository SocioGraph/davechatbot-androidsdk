package com.sociograph.davechatbot.domain.datasources

import com.sociograph.davechatbot.UISetting
import com.sociograph.davechatbot.domain.resmodels.LoginRes


internal interface IAppSettingsDataSource {

    var isLoadedFirstTime: Boolean
    var loginDetail: LoginRes?
    var userId: String
    var engagementId: String?
    var systemResponse: String?

    fun clearLocalStorage()
    val isLogin: Boolean

    var followUps: String

    var waitingTime: Int
    val uiSetting: UISetting

    var pageNumber: Int

}