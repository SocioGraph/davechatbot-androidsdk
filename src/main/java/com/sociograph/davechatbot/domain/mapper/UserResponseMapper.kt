package com.sociograph.davechatbot.domain.mapper

import com.sociograph.davechatbot.domain.entity.MessageItemEntity
import com.sociograph.davechatbot.domain.resmodels.UserRes
import com.sociograph.davechatbot.domain.support_domain.ext.DateFormatType
import com.sociograph.davechatbot.domain.support_domain.ext.parseDate
import com.sociograph.davechatbot.domain.support_domain.mapper.Mapper
import org.joda.time.DateTime


internal class UserResponseMapper : Mapper<UserRes, MessageItemEntity>() {
    override fun map(value: UserRes): MessageItemEntity {
        val messageItemEntity = MessageItemEntity()
        messageItemEntity.let {
            it.response = value
            it.createAt = value.timestamp?.parseDate(DateFormatType.yyyy_MM_dd_hh_mm_ss_a_Z)
                ?: DateTime.now()
            it.isFromMe = true
            it.sequence = value.sequence
        }

        return messageItemEntity
    }

    override fun reverseMap(value: MessageItemEntity): UserRes {
        return value.response as UserRes
    }
}