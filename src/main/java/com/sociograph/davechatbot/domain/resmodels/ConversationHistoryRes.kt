package com.sociograph.davechatbot.domain.resmodels

import com.google.gson.annotations.SerializedName
import com.sociograph.davechatbot.domain.models.MessageItem

class ConversationHistoryRes {

    @field:SerializedName("history")
    var history: ArrayList<MessageItem> = arrayListOf()
}