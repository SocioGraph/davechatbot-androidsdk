package com.sociograph.davechatbot.domain.resmodels

import com.google.gson.annotations.SerializedName
import com.sociograph.davechatbot.domain.models.MessageItem

data class ConversationRes(

    @SerializedName("response_channels")
    var responseChannels: ResponseChannels? = null,

    @SerializedName("whiteboard_title")
    var whiteboardTitle: String? = null,

    @SerializedName("wait")
    var wait: Int = -1,

    @SerializedName("customer_state")
    var customerState: String? = null,

    @SerializedName("data")
    var data: ConversationData? = null,

    @SerializedName("response_id")
    var responseId: String? = null,

    @SerializedName("whiteboard_template")
    var whiteboardTemplate: String? = null,

    @SerializedName("show_feedback")
    var showFeedback: Boolean = false,

    @SerializedName("to_state_function")
    var toStateFunction: ToStateFunction? = null,

    @SerializedName("placeholder_aliases")
    var placeholderAliases: PlaceholderAliases? = null,

    @SerializedName("title")
    var title: String? = null,

    @SerializedName("whiteboard")
    var whiteboard: String? = null,

    @SerializedName("state_options")
    var stateOptions: ArrayList<StateOptions> = arrayListOf(),

    @SerializedName("maintain_whiteboard")
    var maintainWhiteboard: Boolean? = null,

    @SerializedName("name")
    var name: String? = null,

    @SerializedName("options")
    var options: Any? = null,

    @SerializedName("overwrite_whiteboard")
    var overwriteWhiteboard: Boolean? = null,

    @SerializedName("engagement_id")
    var engagementId: String? = null,

    @SerializedName("placeholder")
    var placeholder: String? = null,

    @SerializedName("show_in_history")
    var showInHistory: Boolean? = null,

    @SerializedName("timestamp")
    var timestamp: String? = null,

    @SerializedName("direction")
    var direction: String? = null,

    @SerializedName("sequence")
    var sequence: Int = -1,

    var enableForm: Boolean = true


) : MessageItem {
    override fun toString(): String {
        return "ConversationRes(responseChannels=$responseChannels, whiteboardTitle=$whiteboardTitle, wait=$wait, customerState=$customerState, data=$data, responseId=$responseId, whiteboardTemplate=$whiteboardTemplate, showFeedback=$showFeedback, toStateFunction=$toStateFunction, placeholderAliases=$placeholderAliases, title=$title, whiteboard=$whiteboard, stateOptions=$stateOptions, maintainWhiteboard=$maintainWhiteboard, name=$name, options=$options, overwriteWhiteboard=$overwriteWhiteboard, engagementId=$engagementId, placeholder=$placeholder, showInHistory=$showInHistory, timestamp=$timestamp, enableForm=$enableForm)"
    }
}

data class ResponseChannels(

    @SerializedName("voice")
    var voice: String? = null,

    @SerializedName("frames")
    var frames: String? = null,

    @SerializedName("shapes")
    var shapes: String? = null
)

data class SlideshowItem(

    @SerializedName("image")
    var image: String? = null,

    @SerializedName("caption")
    var caption: String? = null
)

data class FormItem(

    @SerializedName("name")
    var name: String? = null,

    @SerializedName("error")
    var error: String? = null,

    @SerializedName("title")
    var title: String? = null,

    @SerializedName("ui_element")
    var ui_element: String? = null,

    @SerializedName("placeholder")
    var placeholder: String? = null,

    @SerializedName("value")
    var value: String? = null,

    @SerializedName("displayValue")
    var displayValue: String? = null,

    @SerializedName("required")
    var required: Boolean = false,

    @SerializedName("default_date")
    var defaultDate: String? = null,

    @SerializedName("default_time")
    var defaultTime: String? = null,

    @SerializedName("max_date")
    var maxDate: String? = null,

    @SerializedName("max_time")
    var maxTime: String? = null,

    @SerializedName("min_date")
    var minDate: String? = null,

    @SerializedName("min_time")
    var minTime: String? = null,

    @SerializedName("time_resolution")
    var timeResolution: String? = null,

    @SerializedName("file_type")
    var fileType: String? = null,

    @SerializedName("max_file_size")
    var maxFileSize: String? = null,

    @SerializedName("serverPath")
    var serverPath: String = "",

    @SerializedName("options")
    var options: ArrayList<ArrayList<String>> = arrayListOf(),
)

data class ThumbImageItem(
    @SerializedName("image")
    var image: String? = null,

    @SerializedName("title")
    var title: String? = null,

    @SerializedName("url")
    var url: String? = null,
)

data class ConversationData(

    @SerializedName("response_type")
    var responseType: String? = null,

    @SerializedName("customer_state")
    var customerState: String? = null,

    @SerializedName("options")
    var options: ArrayList<Options>? = null,

    @SerializedName("image")
    var image: String? = null,

    @SerializedName("url")
    var url: String? = null,

    @SerializedName("video")
    var video: String? = null,

    @SerializedName("form")
    var form: List<FormItem> = listOf(),

    @SerializedName("thumbnails")
    var thumbnails: List<ThumbImageItem> = listOf(),

    @SerializedName("_follow_ups")
    var followUps: List<String> = listOf()

    /*@SerializedName("options")
    var options: List<String?>? = null,

    @SerializedName("_force_open")
    var forceOpen: Boolean? = null,

    @SerializedName("_open_state")
    var openState: String? = null,

    @SerializedName("response_type")
    var responseType: String? = null,

    @SerializedName("slideshow")
    var slideshow: List<SlideshowItem?>? = null*/
)

data class StateOptions(

    @SerializedName("key")
    var key: String? = null,

    @SerializedName("value")
    var value: String? = null,
)

data class PlaceholderAliases(
    var any: Any? = null
)

data class ToStateFunction(

    @SerializedName("function")
    var function: String? = null
)

data class Options(
    @SerializedName("customer_state")
    var customerState: String = "",

    @SerializedName("customer_response")
    var customerResponse: String = "",
)