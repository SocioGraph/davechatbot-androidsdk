package com.sociograph.davechatbot.domain.resmodels

import com.google.gson.annotations.SerializedName
import java.io.Serializable

internal class BaseRes : Serializable {
    @SerializedName("status")
    var status = 0

    @SerializedName("message")
    var message = ""
    val isSuccess: Boolean
        get() = status == 200
}