package com.sociograph.davechatbot.domain.models


data class LoadingMessageItem(
    var isIncoming: Boolean,
) : MessageItem