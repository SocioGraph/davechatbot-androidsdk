package com.sociograph.davechatbot.domain.ext

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.rxjava2.RxRemoteMediator
import com.sociograph.davechatbot.utils.AppEvent
import com.sociograph.davechatbot.utils.RxBus
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers


@OptIn(ExperimentalPagingApi::class)
internal class GenericRemoteMediator<T : Any>(
    private val getPageKeyForRefresh: () -> Int?,
    private val getPage: (Int) -> Single<List<T>>,
    private val insertAllItems: (items: List<T>) -> Completable
) : RxRemoteMediator<Int, T>() {


    override fun loadSingle(
        loadType: LoadType,
        state: PagingState<Int, T>
    ): Single<MediatorResult> {
        var pageKey = getPageKeyForRefresh.invoke()
        when (loadType) {
            LoadType.REFRESH -> {
                if (pageKey != 1) {
                    RxBus.publish(AppEvent.LoadingInitialHistory(false))
                    return Single.just(MediatorResult.Success(endOfPaginationReached = false))
                } else {
                    RxBus.publish(AppEvent.LoadingInitialHistory(true))
                }
            }
            LoadType.PREPEND -> {
                return Single.just(MediatorResult.Success(endOfPaginationReached = true))
            }
            LoadType.APPEND -> {
                pageKey = getPageKeyForRefresh.invoke() ?: 1
            }
        }

        return getPage(pageKey).subscribeOn(Schedulers.io())
            .map {
                insertAllItems(it).subscribe()
                it
            }
            .map<MediatorResult> {
                if (pageKey == 1) {
                    RxBus.publish(AppEvent.LoadingInitialHistory(false))
                }
                MediatorResult.Success(endOfPaginationReached = it.isEmpty())
            }
            .onErrorReturn {
                if (pageKey == 1) {
                    RxBus.publish(AppEvent.LoadingInitialHistory(false))
                }
                MediatorResult.Error(it)
            }

    }


}
