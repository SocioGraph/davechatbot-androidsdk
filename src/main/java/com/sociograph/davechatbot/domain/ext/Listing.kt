package com.sociograph.davechatbot.domain.ext

import androidx.lifecycle.LiveData
import androidx.paging.PagingData

interface Listing<T : PageItem> {
    fun getDataSource(): LiveData<PagingData<T>>
}