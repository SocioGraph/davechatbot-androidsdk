package com.sociograph.davechatbot.domain.usecase

import com.sociograph.davechatbot.domain.datasources.IAppDataSource
import com.sociograph.davechatbot.domain.datasources.IAppSettingsDataSource
import com.sociograph.davechatbot.domain.resmodels.StateOptions
import com.sociograph.davechatbot.domain.resmodels.UserRes
import com.sociograph.davechatbot.domain.support_domain.usecases.UseCase
import com.sociograph.davechatbot.utils.Constants
import io.reactivex.Single
import java.util.*

internal class SendMessageUseCase constructor(
    private val appDataSource: IAppDataSource,
    private val appSettingsDataSource: IAppSettingsDataSource
) : UseCase<SendMessageUseCase.Params, Single<List<StateOptions>>>() {
    override fun execute(params: Params?): Single<List<StateOptions>> {
        val sendMessageParam = params!!

        val userRes = UserRes(
            customerState = sendMessageParam.customerState,
            customerResponse = sendMessageParam.customerResponse,
            userId = appSettingsDataSource.userId,
            responseId = UUID.randomUUID().toString(),
            userModel = "person",
            direction = Constants.DIRECTION_USER
        )

        userRes.sequence = (appDataSource.getLastMessageFrom()?.sequence ?: 1) + 1

        if (userRes.customerResponse.isNullOrEmpty().not()) {
            appDataSource.insertMessage(userRes)
        }

        return appDataSource.conversation(
            sendMessageParam.conversationId,
            appSettingsDataSource.userId,
            sendMessageParam.customerResponse,
            sendMessageParam.customerState,
            appSettingsDataSource.engagementId,
            appSettingsDataSource.systemResponse,
            sendMessageParam.queryType
        ).map {
            it.direction = Constants.DIRECTION_SYSTEM
            it.sequence = userRes.sequence
            appDataSource.insertMessage(it)
            appSettingsDataSource.systemResponse = it.name
            appSettingsDataSource.engagementId = it.engagementId
            it.data?.followUps?.firstOrNull()?.let {
                appSettingsDataSource.followUps = it
            }
            appSettingsDataSource.waitingTime = it.wait
            it.stateOptions
        }
    }


    class Params(
        val conversationId: String,
        val customerResponse: String?,
        val customerState: String?,
        val queryType: String?
    )
}
