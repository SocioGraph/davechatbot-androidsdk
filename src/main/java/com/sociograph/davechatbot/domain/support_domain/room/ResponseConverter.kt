package com.sociograph.davechatbot.domain.support_domain.room


import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.JsonParser
import com.sociograph.davechatbot.domain.models.MessageItem
import com.sociograph.davechatbot.domain.resmodels.ConversationRes
import com.sociograph.davechatbot.domain.resmodels.UserRes
import com.sociograph.davechatbot.utils.Constants

class ResponseConverter {

    @TypeConverter
    fun toResponse(value: String?): MessageItem? {
        value?.let {
            val jsonObjectItem = JsonParser.parseString(value).asJsonObject
            return if (jsonObjectItem.get("direction").asString == Constants.DIRECTION_SYSTEM) {
                Gson().fromJson(
                    jsonObjectItem,
                    ConversationRes::class.java
                )
            } else {
                Gson().fromJson(jsonObjectItem, UserRes::class.java)
            }
        } ?: run {
            return null
        }
    }

    @TypeConverter
    fun toString(messageItem: MessageItem?): String? {
        return messageItem?.let {
            Gson().toJson(messageItem)
        }?: kotlin.run {
            null
        }
    }
}