package com.sociograph.davechatbot.domain.support_domain

import retrofit2.HttpException
import java.io.IOException

internal interface IAPIExceptionMapper {
    fun decodeHttpException(exception: HttpException): Throwable
    fun decodeUnexpectedException(throwable: Throwable): Throwable
    fun decodeIOException(ioException: IOException): Throwable
}