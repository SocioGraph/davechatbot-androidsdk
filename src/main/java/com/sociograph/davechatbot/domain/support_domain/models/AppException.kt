package com.sociograph.davechatbot.domain.support_domain.models

internal class AppException : Throwable {
    constructor() {}
    constructor(message: String?) : super(message) {}

    var error: MError? = null
    var kind: Kind? = null
    var originalException: Throwable? = null
    var isCritical = false
    var isNetworkTimeout = false

    enum class Kind {
        NETWORK, REST_API, UNEXPECTED
    }
}