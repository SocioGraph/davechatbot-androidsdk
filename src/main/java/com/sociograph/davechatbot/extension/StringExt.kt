package com.sociograph.davechatbot.extension


import android.text.Html
import android.text.Spanned
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.regex.Matcher
import java.util.regex.Pattern


fun String.fromHtml(): Spanned {
    val result: Spanned
    result = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
        Html.fromHtml(convertToHtml(this), Html.FROM_HTML_MODE_LEGACY)
    } else {
        Html.fromHtml(convertToHtml(this))
    }
    return result
}

fun String.fromNormalHtml(): Spanned {
    val result: Spanned
    result = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
        Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY)
    } else {
        Html.fromHtml(this)
    }
    return result
}

private fun convertToHtml(html: String): String {
    return html.replace("<", "&lt;")
        .replace(">", "&gt;")
        .replace("\n", "<br>")
}

fun String.isJSONValid(): Boolean {
    try {
        JSONObject(this)
    } catch (ex: JSONException) {
        try {
            JSONArray(this)
        } catch (ex1: JSONException) {
            return false
        }
    }
    return true
}

fun String.getCapitalizeString(): String {
    val finalTitle = StringBuilder()
    val wordList = this.split(" ")
    if (wordList.isEmpty()) {
        finalTitle.append(this.replaceFirstChar { it.uppercase() })
    } else {
        var space = ""
        wordList.forEach {
            finalTitle.append(space)
            finalTitle.append(it.replaceFirstChar { it.uppercase() })
            space = " "
        }

    }
    return finalTitle.toString()
}


fun String.isValidEmail(): Boolean = if (this.isEmpty()) {
    false
} else {
    val emailPattern =
        "^[A-Za-z\\d\\.\\_\\-\\+]{3,64}\\@([A-Za-z\\d]+)\\.[A-Za-z\\d]+(.[A-Za-z\\d]+)?$"
    val pattern = Pattern.compile(emailPattern)
    val matcher: Matcher = pattern.matcher(this)
    matcher.matches()
}