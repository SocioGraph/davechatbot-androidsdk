package com.sociograph.davechatbot.ui.location_selection

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.google.android.gms.common.api.Status
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.sociograph.davechatbot.R
import com.sociograph.davechatbot.base.BaseDialogFragment
import com.sociograph.davechatbot.databinding.DLocationSelectionBinding
import com.sociograph.davechatbot.extension.isDark
import com.sociograph.davechatbot.extension.obtainViewModel

internal class LocationSelectionDialog : BaseDialogFragment<LocationSelectionViewModel>(),
    OnMapReadyCallback {
    private var selectedLatLng: LatLng? = null
    private var selectedLocationTitle: String? = null
    private lateinit var binding: DLocationSelectionBinding
    private lateinit var mMap: GoogleMap
    var listener: LocationSelectionDialogListener? = null
    override fun initializeViewModel(): LocationSelectionViewModel {
        return obtainViewModel(LocationSelectionViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (showsDialog) {
            setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialogTheme)
        }
    }

    override fun getLayoutView(inflater: LayoutInflater): View {
        binding = DLocationSelectionBinding.inflate(inflater)
        return binding.root
    }

    override fun setUpUI() {
        binding.toolbar.setNavigationIcon(R.drawable.ic_close)
        binding.toolbar.setNavigationOnClickListener {
            dismiss()
        }
        binding.txtTitle.setText(R.string.title_select_location)
        binding.imgDone.setOnClickListener {
            selectedLatLng?.let {
                listener?.onSelectLocation(it, selectedLocationTitle ?: "")
                dismiss()
            } ?: run {
                dismiss()
            }
        }


        setUISetting()
        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        // Initialize the AutocompleteSupportFragment.
        val autocompleteFragment =
            childFragmentManager.findFragmentById(R.id.autocomplete_fragment)
                    as AutocompleteSupportFragment

        autocompleteFragment.setPlaceFields(listOf(Place.Field.ID, Place.Field.NAME,Place.Field.LAT_LNG))

        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
                // TODO: Get info about the selected place.
                Log.i("LocationSelectionDialog", "Place: ${place.name}, ${place.id}")
                place.latLng?.let {
                    mMap.clear()
                    loadMarkerInMap(it, place.name ?: "")
                }

            }

            override fun onError(status: Status) {
                // TODO: Handle the error.
                Log.i("LocationSelectionDialog", "An error occurred: $status")
            }
        })
    }

    private fun setUISetting() {
        val uiSetting = viewModel.uiSetting
        binding.apply {
            toolbar.setBackgroundColor(uiSetting.headerColor)
            val titleColor = ContextCompat.getColor(
                requireContext(),
                if (uiSetting.headerColor.isDark()) R.color.white else R.color.black
            )
            txtTitle.setTextColor(titleColor)
            //(edtSearchLocation.background as GradientDrawable).setStroke(4, uiSetting.headerColor)

        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        // Add a marker in Sydney and move the camera
        //val sydney = LatLng(-34.0, 151.0)
        //loadMarkerInMap(sydney, "Marker in Sydney")
    }

    private fun loadMarkerInMap(latLng: LatLng, title: String) {
        selectedLatLng = latLng
        selectedLocationTitle = title
        mMap.clear()
        mMap.addMarker(
            MarkerOptions()
                .position(latLng)
                .title(title)
        )
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng))
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(CameraPosition.fromLatLngZoom(latLng,14f)))
    }

}

internal interface LocationSelectionDialogListener {
    fun onSelectLocation(latLng: LatLng, title: String)
}