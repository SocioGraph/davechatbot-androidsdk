package com.sociograph.davechatbot.ui.feedback

import android.content.res.ColorStateList
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.fragment.app.DialogFragment
import com.sociograph.davechatbot.R
import com.sociograph.davechatbot.base.BaseDialogFragment
import com.sociograph.davechatbot.databinding.DFeedbackBinding
import com.sociograph.davechatbot.extension.obtainViewModel

internal class FeedbackViewDialog : BaseDialogFragment<FeedbackViewModel>() {
    private lateinit var binding: DFeedbackBinding

    override fun initializeViewModel(): FeedbackViewModel {
        return obtainViewModel(FeedbackViewModel::class.java)
    }

    override fun getLayoutView(inflater: LayoutInflater): View {
        binding = DFeedbackBinding.inflate(inflater)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (showsDialog) {
            setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialogTheme)
        }
    }

    override fun setUpUI() {
        setTheme()

        binding.btnSubmit.setOnClickListener {
            viewModel.submitFeedback(
                binding.rateHelp.rating.toInt().toString(),
                binding.rateAccurate.rating.toInt().toString(),
                binding.edtFeedback.text.toString().trim()
            )
        }

        viewModel.submitFeedbackSuccess.observe(this, {
            showToast("Thank you for your valuable feedback")
            dismiss()
        })
    }

    private fun setTheme() {
        (binding.edtFeedback.background as GradientDrawable)
            .setStroke(4, viewModel.uiSetting.headerColor)

        binding.btnSubmit.setTextColor(viewModel.uiSetting.buttonTextColor)
        binding.btnSubmit.backgroundTintList =
            ColorStateList.valueOf(viewModel.uiSetting.buttonColor)


        binding.rateAccurate.progressTintList =
            ColorStateList.valueOf(viewModel.uiSetting.headerColor)
        binding.rateAccurate.secondaryProgressTintList =
            ColorStateList.valueOf(viewModel.uiSetting.headerColor)

        binding.rateHelp.progressTintList =
            ColorStateList.valueOf(viewModel.uiSetting.headerColor)
        binding.rateHelp.secondaryProgressTintList =
            ColorStateList.valueOf(viewModel.uiSetting.headerColor)
    }


}