package com.sociograph.davechatbot.ui.chat.view_holder

import android.content.res.ColorStateList
import android.view.View
import com.sociograph.davechatbot.R
import com.sociograph.davechatbot.UISetting
import com.sociograph.davechatbot.base.BaseViewHolder
import com.sociograph.davechatbot.databinding.RIncomingOptionsViewBinding
import com.sociograph.davechatbot.domain.entity.MessageItemEntity
import com.sociograph.davechatbot.domain.resmodels.ConversationRes
import com.sociograph.davechatbot.domain.resmodels.Options
import com.sociograph.davechatbot.domain.support_domain.ext.getMessageTime
import com.sociograph.davechatbot.extension.fromNormalHtml
import com.sociograph.davechatbot.interfaces.IItemListener
import com.sociograph.davechatbot.interfaces.MessageAdapterListener
import com.sociograph.davechatbot.ui.chat.adapter.OptionsAdapter

internal class IncomingOptionsMessageViewHolder(
    itemView: View,
    private val messageAdapterListener: MessageAdapterListener?,
    private val uiSetting: UISetting
) :
    BaseViewHolder<MessageItemEntity>(itemView) {

    companion object {
        var LAYOUT_ID = R.layout.r_incoming_options_view
    }

    private val bind = RIncomingOptionsViewBinding.bind(itemView)

    override fun bindViewHolder(item: MessageItemEntity) {
        val conversationItem = item.response as ConversationRes


        bind.llBubble.backgroundTintList = ColorStateList.valueOf(uiSetting.chatBubbleColor)
        bind.incPlainTextView.txtPlainMessage.setTextColor(uiSetting.chatBubbleTextColor)
        bind.imgChatBot.setImageDrawable(uiSetting.chatBotIcon)

        bind.incPlainTextView.txtPlainMessage.text = conversationItem.placeholder?.fromNormalHtml()
        bind.incPlainTextView.txtTime.text = item.createAt.getMessageTime()

        bind.incPlainTextView.txtName.text = uiSetting.titleText
        val optionsAdapter = OptionsAdapter()
        optionsAdapter.setItems(conversationItem.data?.options ?: arrayListOf())
        bind.rvOptions.adapter = optionsAdapter
        optionsAdapter.clickListener = object : IItemListener<Options> {
            override fun onItemClick(position: Int, item: Options) {
                if (item.customerState.isEmpty()) {
                    messageAdapterListener?.onOptionClick(conversationItem.customerState ?: "", item.customerResponse)
                } else {
                    messageAdapterListener?.onOptionClick(item.customerState, item.customerResponse)
                }
            }
        }
    }
}