package com.sociograph.davechatbot.ui.chat.view_holder

import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import com.google.gson.JsonParser
import com.sociograph.davechatbot.R
import com.sociograph.davechatbot.base.BaseViewHolder
import com.sociograph.davechatbot.domain.entity.MessageItemEntity
import com.sociograph.davechatbot.domain.models.UI_ELEMENT
import com.sociograph.davechatbot.domain.resmodels.ConversationRes
import com.sociograph.davechatbot.domain.resmodels.UserRes
import com.sociograph.davechatbot.domain.support_domain.ext.DateFormatType
import com.sociograph.davechatbot.domain.support_domain.ext.getMessageTime
import com.sociograph.davechatbot.domain.support_domain.ext.parseDate
import com.sociograph.davechatbot.extension.fromNormalHtml
import com.sociograph.davechatbot.extension.isJSONValid
import com.sociograph.davechatbot.utils.Constants

abstract class BaseMessageViewHolder(itemView: View) : BaseViewHolder<MessageItemEntity>(itemView) {

    override fun bindViewHolder(item: MessageItemEntity) {

        val parentView = itemView.findViewById<View>(R.id.inc_plain_text_view)
        val messageTextView = parentView.findViewById<AppCompatTextView>(R.id.txtPlainMessage)
        val response = item.response
        if (response is UserRes) {
            messageTextView.text =
                getDisplayResponse(response.customerResponse).fromNormalHtml()
        } else if (response is ConversationRes) {
            messageTextView.text =
                getDisplayResponse(response.placeholder).fromNormalHtml()
        }

        parentView.findViewById<AppCompatTextView>(R.id.txtTime).text =
            item.createAt.getMessageTime()
        val txtName = parentView.findViewById<AppCompatTextView>(R.id.txtName)
        if (item.isFromMe) {
            txtName.text = "You"
        } else {
            txtName.text = getString(R.string.lbl_assistant_name)
        }

    }


    private fun getDisplayResponse(response: String?): String {
        if (response != null && response.isJSONValid()) {
            val displayMessage = StringBuffer()
            val jsonObject = JsonParser.parseString(response).asJsonObject
            jsonObject.entrySet().forEach {
                if (it.value.isJsonPrimitive) {
                    val itemValue = it.value.asString
                    val title =
                        jsonObject.getAsJsonObject(Constants.STR_NAME_MAP).get(it.key).asString
                    val type =
                        jsonObject.getAsJsonObject(Constants.STR_TYPE_MAP).get(it.key).asString


                    displayMessage.append("<b>")
                    displayMessage.append(title)
                    displayMessage.append(":")
                    displayMessage.append("</b> ")
                    when (type) {
                        UI_ELEMENT.DATE.value -> {
                            val dateValue = itemValue?.parseDate(
                                DateFormatType.dd_MM_yyyy,
                                DateFormatType.EEE_MMM_dd_yyyy_HH_mm_ss_zz_zzzz
                            )
                            displayMessage.append(dateValue)
                        }
                        UI_ELEMENT.DATETIME.value -> {
                            val dateValue = itemValue?.parseDate(
                                DateFormatType.dd_MM_yyyy_HH_mm,
                                DateFormatType.MMM_dd_yyyy_HH_mm
                            )
                            displayMessage.append(dateValue)
                        }
                        else -> {
                            displayMessage.append(itemValue)
                        }
                    }

                    displayMessage.append("<br> ")
                }
            }
            return displayMessage.toString()
        } else {
            return response ?: ""
        }
    }
}