package com.sociograph.davechatbot.ui.chat

import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.drawable.GradientDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.webkit.MimeTypeMap
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.documentfile.provider.DocumentFile
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.google.android.libraries.places.api.Places
import com.sociograph.davechatbot.DaveChatBotSdk
import com.sociograph.davechatbot.R
import com.sociograph.davechatbot.base.BaseActivity
import com.sociograph.davechatbot.core.ConfigData
import com.sociograph.davechatbot.databinding.FChatViewBinding
import com.sociograph.davechatbot.databinding.RFormFileInputViewBinding
import com.sociograph.davechatbot.domain.models.KeywordItem
import com.sociograph.davechatbot.domain.models.LoadingMessageItem
import com.sociograph.davechatbot.domain.resmodels.ConversationRes
import com.sociograph.davechatbot.domain.resmodels.FormItem
import com.sociograph.davechatbot.domain.resmodels.StateOptions
import com.sociograph.davechatbot.domain.resmodels.ThumbImageItem
import com.sociograph.davechatbot.domain.support_domain.schedulers.Scheduler
import com.sociograph.davechatbot.extension.isDark
import com.sociograph.davechatbot.extension.loadImage
import com.sociograph.davechatbot.extension.nameAndLength
import com.sociograph.davechatbot.extension.obtainViewModel
import com.sociograph.davechatbot.interfaces.IItemListener
import com.sociograph.davechatbot.interfaces.MessageAdapterListener
import com.sociograph.davechatbot.ui.chat.adapter.LoadingMessageAdapter
import com.sociograph.davechatbot.ui.chat.adapter.MessagePagedAdapter
import com.sociograph.davechatbot.ui.chat.adapter.PredictAdapter
import com.sociograph.davechatbot.ui.chat.adapter.StateOptionsAdapter
import com.sociograph.davechatbot.utils.FileUtils
import com.sociograph.davechatbot.utils.FileUtils.sizeInMb


internal class ChatViewActivity : BaseActivity<ChatViewModel>() {


    companion object {
        fun startActivity(context: Context) {
            context.startActivity(Intent(context, ChatViewActivity::class.java))
        }
    }

    private lateinit var predictedAdapter: PredictAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var messageAdapter: MessagePagedAdapter
    private lateinit var loadingMessageAdapter: LoadingMessageAdapter
    private lateinit var binding: FChatViewBinding
    private lateinit var stateOptionsAdapter: StateOptionsAdapter

    override fun initializeViewModel(): ChatViewModel {
        return obtainViewModel(ChatViewModel::class.java)
    }

    override fun getLayoutView(): View {
        binding = FChatViewBinding.inflate(layoutInflater)
        return binding.root
    }


    override fun setUpChildUI(savedInstanceState: Bundle?) {
        initializePlaces()
        setUISetting()
        viewModel.subscribe()
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        stateOptionsAdapter = StateOptionsAdapter(viewModel.uiSetting)
        binding.rvOption.adapter = stateOptionsAdapter


        linearLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, true)
        //linearLayoutManager.stackFromEnd = false
        binding.rvMessage.layoutManager = linearLayoutManager

        messageAdapter = MessagePagedAdapter(viewModel.uiSetting)
        loadingMessageAdapter = LoadingMessageAdapter(viewModel.uiSetting)
        binding.rvMessage.adapter = ConcatAdapter(loadingMessageAdapter, messageAdapter)

        val itemAnimator = binding.rvMessage.itemAnimator
        if (itemAnimator is SimpleItemAnimator) {
            itemAnimator.supportsChangeAnimations = false
        }

        predictedAdapter = PredictAdapter()
        binding.rvPredictedList.adapter = predictedAdapter
        attachListeners()
        setUpObservers()
    }

    private fun setUISetting() {
        val uiSetting = viewModel.uiSetting
        binding.apply {
            root.setBackgroundColor(uiSetting.backgroundViewColor)
            flProgress.setBackgroundColor(uiSetting.backgroundViewColor)
            imgProfile.setImageDrawable(uiSetting.chatBotIcon)
            toolbar.setBackgroundColor(uiSetting.headerColor)
            txtOptions.text = uiSetting.optionsText
            txtTitle.text = uiSetting.titleText
            val titleColor = ContextCompat.getColor(
                this@ChatViewActivity,
                if (uiSetting.headerColor.isDark()) R.color.white else R.color.black
            )
            txtTitle.setTextColor(titleColor)

            (llMessage.background as GradientDrawable).setStroke(4, uiSetting.headerColor)

            imgSend.setImageDrawable(uiSetting.sendIcon)
            imgSend.backgroundTintList = ColorStateList.valueOf(uiSetting.headerColor)
        }
    }

    private fun attachListeners() {
        binding.imgSend.setOnClickListener {
            if (getCustomerResponse().isNotEmpty()) {
                viewModel.sendMessage(getCustomerResponse(), null, queryType = "type")
                binding.edtMessage.editableText.clear()
            }
        }

        stateOptionsAdapter.clickListener = object : IItemListener<StateOptions> {
            override fun onItemClick(position: Int, item: StateOptions) {
                viewModel.sendMessage(item.value, item.key, queryType = "click")
            }
        }
        predictedAdapter.clickListener = object : IItemListener<KeywordItem> {
            override fun onItemClick(position: Int, item: KeywordItem) {
                viewModel.sendMessage(
                    item.customerResponse,
                    item.customerState,
                    queryType = "click"
                )
                binding.edtMessage.editableText.clear()
            }
        }

        binding.edtMessage.addTextChangedListener {
            if (it.isNullOrEmpty()) {
                binding.rvPredictedList.isVisible = false
                predictedAdapter.clearItems()
                viewModel.hideLoadingView(false)
            } else {
                viewModel.showPredictedList(it.toString())
                viewModel.showLoadingView(false)
            }
        }

        messageAdapter.messageAdapterListener = object : MessageAdapterListener {
            override fun onOptionClick(state: String, option: String) {
                viewModel.sendMessage(option, state, queryType = "click")
            }

            override fun onSubmitForm(id: Long, customerState: String?, item: List<FormItem>) {
                viewModel.submitForm(id, customerState, item)
            }

            override fun onImageClick(item: ThumbImageItem) {
                openUrl(item.url)
            }

            override fun onChooseFileClick(
                bind: RFormFileInputViewBinding,
                bindingAdapterPosition: Int,
                conversationItem: ConversationRes
            ) {
                requestAndChooseFile(
                    FormSelectionItem(
                        bind,
                        bindingAdapterPosition,
                        conversationItem
                    )
                )
            }

            override fun getFilledConversationData(responseId: String): ConversationRes? {
                return formMapData[responseId]
            }

            override fun setFilledConversationData(
                conversationRes: ConversationRes
            ) {
                conversationRes.responseId?.let {
                    formMapData[it] = conversationRes
                }
            }

            override fun onSubmitFeedback(responseId: String, response: String) {
                viewModel.submitFeedback(responseId, response)
            }

            override fun getFragmentManager(): FragmentManager {
                return supportFragmentManager
            }

        }
    }

    private fun getCustomerResponse(): String {
        return binding.edtMessage.text.toString().trim()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_chat, menu)
        menu?.findItem(R.id.action_close)?.icon = viewModel.uiSetting.closeIcon
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_close -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setUpObservers() {
        viewModel.stateOptionsListLive.observe(this, {
            binding.llOptions.isVisible = it.isNotEmpty()
            stateOptionsAdapter.setItems(it.toMutableList())
        })

        viewModel.showLoadingView.observe(this, { isIncoming ->
            showLoadingView(isIncoming)
        })

        viewModel.hideLoadingView.observe(this, { isIncoming ->
            hideLoadingView(isIncoming)
        })

        viewModel.dataSource.observe(this, { messages ->
            messageAdapter.submitData(lifecycle, messages)
        })

        messageAdapter.registerAdapterDataObserver(object :
            RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                super.onItemRangeInserted(positionStart, itemCount)
                if (linearLayoutManager.findFirstVisibleItemPosition() == 0) {
                    binding.rvMessage.scrollToPosition(0)
                }
            }
        })

        viewModel.historyProgressView.observe(this, {
            binding.flProgress.isVisible = it
            if (it) {
                binding.imgLoading.loadImage(viewModel.uiSetting.loaderImage)
            }
        })

        viewModel.showPredictedListLive.observe(this, {
            binding.rvPredictedList.isVisible = it.isNotEmpty()
            predictedAdapter.setItems(it.toMutableList())
        })

        viewModel.submitFeedbackSuccess.observe(this, {
            showToast("Thank you for your valuable feedback")
        })
    }

    private fun hideLoadingView(isIncoming: Boolean) {
        val indexOfFirst = loadingMessageAdapter.getItems()
            .indexOfFirst { it.isIncoming == isIncoming }
        if (indexOfFirst != -1) {
            loadingMessageAdapter.remove(indexOfFirst)
        }
    }

    private fun showLoadingView(isIncoming: Boolean) {
        if (loadingMessageAdapter.getItems()
                .any { it.isIncoming == isIncoming }
        ) {
            return
        }
        loadingMessageAdapter.add(0, LoadingMessageItem(isIncoming))
        binding.rvMessage.smoothScrollToPosition(0)
    }

    override fun onDestroy() {
        super.onDestroy()
        //viewModel.clearEngagementId()
    }


    private fun openUrl(url: String?) {
        url?.let {
            val myIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            startActivity(myIntent)
        }
    }

    private val formMapData: HashMap<String, ConversationRes> = hashMapOf()

    private val getContent =
        registerForActivityResult(ActivityResultContracts.OpenDocument()) { uri: Uri? ->
            selectedFormSelectionItem?.let { formSelectionItem ->
                val selectedFormItem =
                    formSelectionItem.conversationItem.data?.form?.get(formSelectionItem.position)
                val maxSize = selectedFormItem?.maxFileSize?.toDouble() ?: 10.0
                uri?.let {
                    val fromSingleUri = DocumentFile.fromSingleUri(this, uri)
                    var name = ""
                    var length = 0.0
                    fromSingleUri?.let {
                        name = it.name ?: ""
                        length = it.length().toDouble()
                    } ?: run {
                        val nameAndLength = uri.nameAndLength(contentResolver)
                        name = nameAndLength.first
                        length = nameAndLength.second.toDouble()
                    }
                    selectedFormItem?.let {
                        showCustomProgressDialog()
                        if (length.sizeInMb <= maxSize) {
                            viewModel.addRxCall(
                                FileUtils.copyFileToTemp(uri, name)
                                    .subscribeOn(Scheduler.io())
                                    .observeOn(Scheduler.ui())
                                    .subscribe({
                                        hideCustomProgressDialog()
                                        selectedFormItem.value = it.absolutePath
                                        formSelectionItem.bind.txtFileName.text = it.name
                                        formMapData[formSelectionItem.conversationItem.responseId
                                            ?: ""] =
                                            formSelectionItem.conversationItem
                                    }, {
                                        hideCustomProgressDialog()
                                        showToast(R.string.msg_something_went_wrong_try_again)
                                    })
                            )
                        } else {
                            hideCustomProgressDialog()
                            showToast(getString(R.string.msg_file_size_must_be_less_then, maxSize))
                        }
                    }
                }
            }
        }

    private var selectedFormSelectionItem: FormSelectionItem? = null

    private fun requestAndChooseFile(
        formSelectionItem: FormSelectionItem,
    ) {
        val item = formSelectionItem.conversationItem.data?.form?.get(formSelectionItem.position)
        val supportedTypeList = hashSetOf<String>()
        item?.fileType?.split(",")?.forEach {
            if (it.startsWith(".")) {
                MimeTypeMap.getSingleton().getMimeTypeFromExtension(it.removePrefix("."))?.let {
                    supportedTypeList.add(it)
                }
            } else if (it.contains("/*")) {
                supportedTypeList.add(it)
            }
        }

        requestStoragePermission { isGranted ->
            if (isGranted) {
                selectedFormSelectionItem = formSelectionItem
                getContent.launch(supportedTypeList.toTypedArray())
            }
        }
    }

    data class FormSelectionItem(
        val bind: RFormFileInputViewBinding,
        val position: Int,
        val conversationItem: ConversationRes
    )

    override fun onUserInteraction() {
        super.onUserInteraction()
        Log.w("CountDownTimer", "onUserInteraction")
        viewModel.resetCountDownTimer()
    }

    override fun onPause() {
        super.onPause()
        viewModel.cancelTimer()
    }

    private fun initializePlaces() {
        // Initialize the SDK
        try {
            if (ConfigData.GOOGLE_API_KEY.trim().isNotEmpty()) {
                Places.initialize(DaveChatBotSdk.appContext, ConfigData.GOOGLE_API_KEY)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}