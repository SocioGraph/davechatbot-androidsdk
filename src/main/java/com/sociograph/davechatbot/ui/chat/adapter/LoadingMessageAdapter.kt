package com.sociograph.davechatbot.ui.chat.adapter

import android.view.View
import com.sociograph.davechatbot.UISetting
import com.sociograph.davechatbot.base.BaseRecyclerViewAdapter
import com.sociograph.davechatbot.base.BaseViewHolder
import com.sociograph.davechatbot.domain.models.LoadingMessageItem
import com.sociograph.davechatbot.ui.chat.view_holder.LoadingMessageViewHolder

internal class LoadingMessageAdapter(private val uiSetting: UISetting) :
    BaseRecyclerViewAdapter<LoadingMessageItem, BaseViewHolder<LoadingMessageItem>>() {


    override fun getRowLayoutId(viewType: Int): Int {
        return viewType
    }

    override fun getViewHolder(view: View, viewType: Int): BaseViewHolder<LoadingMessageItem> {
        return LoadingMessageViewHolder(view, uiSetting)
    }

    override fun getItemViewType(position: Int): Int {
        return if (dataList[position].isIncoming) {
            LoadingMessageViewHolder.INCOMING_LAYOUT_ID
        } else {
            LoadingMessageViewHolder.OUTGOING_LAYOUT_ID
        }
    }


    override fun bind(
        viewHolder: BaseViewHolder<LoadingMessageItem>,
        position: Int,
        item: LoadingMessageItem
    ) {
        viewHolder.bindViewHolder(item)
    }

}
