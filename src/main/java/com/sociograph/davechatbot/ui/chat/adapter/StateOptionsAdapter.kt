package com.sociograph.davechatbot.ui.chat.adapter

import android.content.res.ColorStateList
import android.view.View
import com.sociograph.davechatbot.R
import com.sociograph.davechatbot.UISetting
import com.sociograph.davechatbot.base.BaseRecyclerViewAdapter
import com.sociograph.davechatbot.base.BaseViewHolder
import com.sociograph.davechatbot.databinding.RStateOptionViewBinding
import com.sociograph.davechatbot.domain.resmodels.StateOptions

internal class StateOptionsAdapter(private val uiSetting: UISetting) :
    BaseRecyclerViewAdapter<StateOptions, StateOptionsAdapter.StateOptionsViewHolder>() {


    override fun getRowLayoutId(viewType: Int): Int {
        return R.layout.r_state_option_view
    }

    override fun getViewHolder(view: View, viewType: Int): StateOptionsViewHolder {
        return StateOptionsViewHolder(view)
    }

    override fun bind(
        viewHolder: StateOptionsViewHolder,
        position: Int,
        item: StateOptions
    ) {
        viewHolder.bindViewHolder(item)
    }


    inner class StateOptionsViewHolder(itemView: View) :
        BaseViewHolder<StateOptions>(itemView) {

        private val bind = RStateOptionViewBinding.bind(itemView)

        override fun bindViewHolder(item: StateOptions) {

            bind.txtOptions.backgroundTintList = ColorStateList.valueOf(uiSetting.chatBubbleColor)
            bind.txtOptions.setTextColor(uiSetting.chatBubbleTextColor)

            bind.txtOptions.text = item.value
            bind.txtOptions.setOnClickListener {
                clickListener?.onItemClick(bindingAdapterPosition, item)
            }
        }
    }

}