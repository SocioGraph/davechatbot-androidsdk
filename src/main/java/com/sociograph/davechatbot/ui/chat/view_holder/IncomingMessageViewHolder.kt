package com.sociograph.davechatbot.ui.chat.view_holder

import android.content.res.ColorStateList
import android.view.View
import androidx.core.view.isVisible
import com.sociograph.davechatbot.R
import com.sociograph.davechatbot.UISetting
import com.sociograph.davechatbot.base.BaseViewHolder
import com.sociograph.davechatbot.databinding.RIncomingPlainTextViewBinding
import com.sociograph.davechatbot.domain.entity.MessageItemEntity
import com.sociograph.davechatbot.domain.resmodels.ConversationRes
import com.sociograph.davechatbot.domain.support_domain.ext.getMessageTime
import com.sociograph.davechatbot.extension.fromNormalHtml
import com.sociograph.davechatbot.interfaces.MessageAdapterListener

internal class IncomingMessageViewHolder(
    itemView: View, private val messageAdapterListener: MessageAdapterListener?,
    private val uiSetting: UISetting
) :
    BaseViewHolder<MessageItemEntity>(itemView) {

    companion object {
        var LAYOUT_ID = R.layout.r_incoming_plain_text_view
    }

    private val bind = RIncomingPlainTextViewBinding.bind(itemView)

    override fun bindViewHolder(item: MessageItemEntity) {
        val conversationItem = item.response as ConversationRes


        bind.llBubble.backgroundTintList = ColorStateList.valueOf(uiSetting.chatBubbleColor)
        bind.incPlainTextView.txtPlainMessage.setTextColor(uiSetting.chatBubbleTextColor)
        bind.imgChatBot.setImageDrawable(uiSetting.chatBotIcon)

        bind.incPlainTextView.txtPlainMessage.text = conversationItem.placeholder?.fromNormalHtml()
        bind.incPlainTextView.txtTime.text = item.createAt.getMessageTime()
        bind.incPlainTextView.txtName.text = uiSetting.titleText

        bind.llFeedback.isVisible = conversationItem.showFeedback

        bind.imgThumbDown.setOnClickListener {
            messageAdapterListener?.onSubmitFeedback(conversationItem.responseId ?: "", "1")
        }
        bind.imgThumbUp.setOnClickListener {
            messageAdapterListener?.onSubmitFeedback(conversationItem.responseId ?: "", "5")
        }
    }
}