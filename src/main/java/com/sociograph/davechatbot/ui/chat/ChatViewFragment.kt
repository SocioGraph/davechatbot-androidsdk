package com.sociograph.davechatbot.ui.chat

/*
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.DialogFragment
import com.sociograph.davechatbot.R
import com.sociograph.davechatbot.base.BaseDialogFragment
import com.sociograph.davechatbot.databinding.FChatViewBinding
import com.sociograph.davechatbot.domain.models.LoadingMessageItem
import com.sociograph.davechatbot.domain.models.MessageItem
import com.sociograph.davechatbot.domain.resmodels.StateOptions
import com.sociograph.davechatbot.extension.obtainViewModel
import com.sociograph.davechatbot.interfaces.IItemListener
import com.sociograph.davechatbot.interfaces.MessageAdapterListener
import com.sociograph.davechatbot.ui.chat.adapter.MessageAdapter
import com.sociograph.davechatbot.ui.chat.adapter.StateOptionsAdapter

internal class ChatViewFragment : BaseDialogFragment<ChatViewModel>() {


    companion object {
        fun newInstance(): ChatViewFragment {
            return ChatViewFragment()
        }
    }

    private lateinit var messageAdapter: MessageAdapter
    private lateinit var binding: FChatViewBinding
    private var isFullScreen = false
    private lateinit var stateOptionsAdapter: StateOptionsAdapter

    override fun initializeViewModel(): ChatViewModel {
        return obtainViewModel(ChatViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (showsDialog) {
            setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomDialogTheme)
        }
    }

    override fun getLayoutView(inflater: LayoutInflater): View? {
        binding = FChatViewBinding.inflate(inflater)
        return binding.root
    }

    override fun setUpUI() {
        viewModel.subscribe()
        binding.toolbar.inflateMenu(R.menu.menu_chat)

        binding.txtTitle.text = getString(R.string.lbl_assistant_name)

        stateOptionsAdapter = StateOptionsAdapter()
        binding.rvOption.adapter = stateOptionsAdapter


        */
/*val linearLayoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, true)
        //linearLayoutManager.stackFromEnd = false
        binding.rvMessage.layoutManager = linearLayoutManager*//*


        messageAdapter = MessageAdapter()
        binding.rvMessage.adapter = messageAdapter

        attachListeners()
        setUpObservers()
    }

    private fun attachListeners() {
        binding.toolbar.setOnMenuItemClickListener { item ->
            when (item?.itemId) {
                R.id.action_close -> {
                    dismiss()
                }
                R.id.action_toggle_full_screen -> {
                    if (isFullScreen) {
                        closeFullScreen()
                    } else {
                        showFullScreen()
                    }
                    isFullScreen = !isFullScreen
                    binding.toolbar.menu?.findItem(R.id.action_toggle_full_screen)
                        ?.setIcon(
                            if (isFullScreen)
                                R.drawable.ic_close_fullscreen
                            else
                                R.drawable.ic_open_full_screen
                        )
                }
            }
            false
        }

        binding.imgSend.setOnClickListener {
            if (getCustomerResponse().isNotEmpty()) {
                viewModel.sendMessage(getCustomerResponse(), null)
                binding.edtMessage.editableText.clear()
            }
        }

        stateOptionsAdapter.clickListener = object : IItemListener<StateOptions> {
            override fun onItemClick(position: Int, item: StateOptions) {
                viewModel.sendMessage(item.value, item.key)
            }
        }

        binding.edtMessage.addTextChangedListener {
            if (it.isNullOrEmpty()) {
                viewModel.hideLoadingView(false)
            } else {
                viewModel.showOutgoingLoadingView()
            }
        }

        messageAdapter.messageAdapterListener = object : MessageAdapterListener {
            override fun onOptionClick(state: String, option: String) {
                viewModel.sendMessage(option, state)
            }
        }
    }

    private fun closeFullScreen() {
        val height = (resources.displayMetrics.heightPixels * 0.50).toInt()
        dialog?.window?.setLayout(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            height
        )
    }

    private fun showFullScreen() {
        dialog?.window?.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
    }

    */
/*override fun onStart() {
        super.onStart()
        *//*
*/
/*val width = (resources.displayMetrics.widthPixels * 0.90).toInt()
        dialog?.window?.setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT)*//*
*/
/*
        val height = (resources.displayMetrics.heightPixels * 0.50).toInt()
        dialog?.window?.setLayout(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            height
        )
    }*//*


    private fun getCustomerResponse(): String {
        return binding.edtMessage.text.toString().trim()
    }

    private fun getCustomerState(): String? {
        return null
    }

    private fun setUpObservers() {
        viewModel.stateOptionsListLive.observe(this, {
            binding.llOptions.isVisible = it.isNotEmpty()
            stateOptionsAdapter.setItems(it.toMutableList())
        })

        viewModel.addMessageLive.observe(this, { messageItem ->
            addNewMessage(messageItem)
        })

        viewModel.hideLoadingView.observe(this, { isIncoming ->
            hideLoadingView(isIncoming)
        })
    }

    private fun hideLoadingView(isIncoming: Boolean) {
        val indexOfFirst = messageAdapter.getItems()
            .indexOfFirst { it is LoadingMessageItem && it.isIncoming == isIncoming }
        if (indexOfFirst != -1) {
            messageAdapter.remove(indexOfFirst)
        }
    }

    private fun addNewMessage(messageItem: MessageItem) {
        if (messageItem is LoadingMessageItem && messageAdapter.getItems()
                .any { it is LoadingMessageItem && it.isIncoming == messageItem.isIncoming }
        ) {
            return
        }
        messageAdapter.add(0, messageItem)
        binding.rvMessage.smoothScrollToPosition(0)
    }
}*/
