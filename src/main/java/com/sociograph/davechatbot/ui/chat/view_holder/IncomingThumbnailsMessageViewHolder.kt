package com.sociograph.davechatbot.ui.chat.view_holder

import android.content.res.ColorStateList
import android.view.View
import com.sociograph.davechatbot.R
import com.sociograph.davechatbot.UISetting
import com.sociograph.davechatbot.base.BaseViewHolder
import com.sociograph.davechatbot.databinding.RIncomingThumbnailsViewBinding
import com.sociograph.davechatbot.domain.entity.MessageItemEntity
import com.sociograph.davechatbot.domain.resmodels.ConversationRes
import com.sociograph.davechatbot.domain.resmodels.ThumbImageItem
import com.sociograph.davechatbot.domain.support_domain.ext.getMessageTime
import com.sociograph.davechatbot.extension.fromNormalHtml
import com.sociograph.davechatbot.interfaces.IItemListener
import com.sociograph.davechatbot.interfaces.MessageAdapterListener
import com.sociograph.davechatbot.ui.chat.adapter.ThumbnailImageAdapter

internal class IncomingThumbnailsMessageViewHolder(
    itemView: View,
    private val messageAdapterListener: MessageAdapterListener?,
    private val uiSetting: UISetting
) :
    BaseViewHolder<MessageItemEntity>(itemView) {

    companion object {
        var LAYOUT_ID = R.layout.r_incoming_thumbnails_view
    }

    private val bind = RIncomingThumbnailsViewBinding.bind(itemView)

    override fun bindViewHolder(item: MessageItemEntity) {
        val conversationItem = item.response as ConversationRes


        bind.llBubble.backgroundTintList = ColorStateList.valueOf(uiSetting.chatBubbleColor)
        bind.incPlainTextView.txtPlainMessage.setTextColor(uiSetting.chatBubbleTextColor)
        bind.imgChatBot.setImageDrawable(uiSetting.chatBotIcon)

        bind.incPlainTextView.txtPlainMessage.text = conversationItem.placeholder?.fromNormalHtml()
        bind.incPlainTextView.txtTime.text = item.createAt.getMessageTime()
        bind.incPlainTextView.txtName.text = uiSetting.titleText
        val thumbnailImageAdapter = ThumbnailImageAdapter()
        thumbnailImageAdapter.setItems(
            conversationItem.data?.thumbnails?.toMutableList() ?: mutableListOf()
        )
        thumbnailImageAdapter.clickListener = object : IItemListener<ThumbImageItem> {
            override fun onItemClick(position: Int, item: ThumbImageItem) {
                messageAdapterListener?.onImageClick(item)
            }

        }
        bind.rvThumbnailImages.adapter = thumbnailImageAdapter
    }
}