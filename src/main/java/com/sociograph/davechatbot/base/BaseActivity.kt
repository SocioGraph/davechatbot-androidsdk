package com.sociograph.davechatbot.base


import android.Manifest
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.sociograph.davechatbot.R
import com.sociograph.davechatbot.databinding.ABaseLayoutBinding
import com.sociograph.davechatbot.utils.EasyPermissions
import com.sociograph.davechatbot.widget.CustomProgressDialog


abstract class BaseActivity<VM : BaseViewModel> : AppCompatActivity() {

    private var baseBinding: ABaseLayoutBinding? = null
    private val TAG = this::class.java.simpleName

    protected lateinit var viewModel: VM
    protected abstract fun initializeViewModel(): VM
    private lateinit var customProgressDialog: CustomProgressDialog

    companion object {
        const val REQ_STORAGE_CAMERA_PERMISSION = 121
        const val REQ_STORAGE_PERMISSION = 122
    }

    protected abstract fun getLayoutView(): View?

    protected abstract fun setUpChildUI(savedInstanceState: Bundle?)

    open fun readIntent() {}

    protected open fun getProgressView(): View? {
        return baseBinding?.vContentLoadProgress
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        baseBinding = ABaseLayoutBinding.inflate(layoutInflater)
        setContentView(baseBinding?.root)

        baseBinding?.aBaseLayoutContent?.addView(getLayoutView())

        viewModel = initializeViewModel()
        setBaseUpObserver()


        readIntent()
        setUpChildUI(savedInstanceState)
    }

    private fun setBaseUpObserver() {
        viewModel.showProgress.observe(this, { show ->
            if (show) {
                showProgress()
            } else {
                hideDataProgress()
            }
        })

        viewModel.showDataProgress.observe(this, { show ->
            if (show) {
                showDataProgress()
            } else {
                hideDataProgress()
            }
        })

        viewModel.showProgressDialog.observe(this, { show ->
            if (show) {
                showCustomProgressDialog()
            } else {
                hideCustomProgressDialog()
            }
        })

        viewModel.showSoftMessage.observe(this, { message ->
            showToast(message)
        })
    }

    public fun showDataProgress() {
        getProgressView()?.setBackgroundColor(
            ContextCompat.getColor(
                this,
                R.color.white
            )
        )
        getProgressView()?.visibility = View.VISIBLE
    }

    public fun hideDataProgress() {
        getProgressView()?.setBackgroundColor(
            ContextCompat.getColor(
                this,
                android.R.color.transparent
            )
        )
        getProgressView()?.visibility = View.GONE
    }

    fun showProgress() {
        getProgressView()?.visibility = View.VISIBLE
    }

    fun hideProgress() {
        getProgressView()?.visibility = View.GONE
    }

    fun hideSoftKeyboard() {
        val view = currentFocus
        if (view != null) {
            val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(
                view.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
        }
    }


    fun showMessage(@StringRes resId: Int) {
        showMessage(true, getString(resId))
    }

    fun showMessage(message: String) {
        showMessage(true, message)
    }

    fun showMessage(isSuccess: Boolean, @StringRes resId: Int) {
        showMessage(isSuccess, getString(resId))
    }

    fun showToast(@StringRes resId: Int) {
        showToast(getString(resId))
    }

    fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    fun showMessage(isSuccess: Boolean, message: String) {
        val snackbar =
            Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG)
        val textView =
            snackbar.view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
        textView.setTextColor(if (isSuccess) Color.GREEN else Color.WHITE)
        textView.maxLines = 3
        snackbar.show()
    }

    /**
     * CHECK INTERNET CONNECTIVITY.
     */

    fun isInternetAvailable(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm?.activeNetworkInfo
        return netInfo != null && netInfo.isConnected && netInfo.isAvailable
    }

    fun showNoInternetAvailable() {
        val snackbar = Snackbar.make(
            findViewById(android.R.id.content),
            getString(R.string.msg_no_internet_available),
            Snackbar.LENGTH_LONG
        )
        val textView =
            snackbar.view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
        textView.setTextColor(Color.RED)
        snackbar.show()
    }

    private fun requestStoragePermission(
        listener: (isGranted: Boolean) -> Unit,
        @StringRes infoMessage: Int
    ) {
        if (!EasyPermissions.hasPermissions(
                this, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
        ) {
            showExplanationDialog(
                infoMessage
            ) { dialogInterface, _ ->
                dialogInterface.dismiss()
                EasyPermissions.requestPermissions(
                    this,
                    REQ_STORAGE_PERMISSION,
                    object : EasyPermissions.PermissionCallbacks {
                        override fun onPermissionsGranted(
                            requestCode: Int,
                            perms: List<String>
                        ) {
                            listener(true)
                        }

                        override fun onPermissionsDenied(
                            requestCode: Int,
                            perms: List<String>
                        ) {
                            listener(false)
                        }

                        override fun onPermissionsPermanentlyDeclined(
                            requestCode: Int,
                            perms: List<String>
                        ) {
                            showPermissionSettingDialog()
                        }

                    },
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                )
            }

        } else {
            listener(true)
        }
    }

    fun requestStoragePermission(listener: (isGranted: Boolean) -> Unit) {
        requestStoragePermission(listener, R.string.msg_storage_permission)
    }

    fun showPermissionSettingDialog() {
        val alertBuilder = AlertDialog.Builder(this)
        alertBuilder.setTitle("Permission necessary")
        alertBuilder.setMessage(getString(R.string.msg_permission_step))
        alertBuilder.setPositiveButton(R.string.dialog_ok) { dialogInterface, i ->
            dialogInterface.dismiss()
            val intent = Intent()
            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            val uri = Uri.fromParts("package", packageName, null)
            intent.data = uri
            startActivity(intent)
        }
        alertBuilder.setNegativeButton(R.string.dialog_cancel) { dialogInterface, i ->
            dialogInterface.dismiss()
        }

        val alert = alertBuilder.create()
        alert.setCancelable(false)
        alert.show()
    }

    private fun showExplanationDialog(
        @StringRes infoMessage: Int,
        onClickListener: DialogInterface.OnClickListener
    ) {
        val alertBuilder = AlertDialog.Builder(this)
        alertBuilder.setTitle("Permission necessary")
        alertBuilder.setMessage(getString(infoMessage))
        alertBuilder.setPositiveButton(R.string.dialog_continue, onClickListener)
        val alert = alertBuilder.create()
        alert.setCancelable(false)
        alert.show()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    fun showCustomProgressDialog() {
        showCustomProgressDialog(false)
    }

    fun showCustomProgressDialog(canCancel: Boolean) {
        customProgressDialog = CustomProgressDialog(this)
        customProgressDialog.show()
        customProgressDialog.setCancelable(canCancel)
    }

    fun hideCustomProgressDialog() {
        if (::customProgressDialog.isInitialized && customProgressDialog.isShowing() && !isDestroyed) {
            customProgressDialog.dismiss()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.unsubscribe()
    }
}
