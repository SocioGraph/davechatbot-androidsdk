package com.sociograph.davechatbot

import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.util.Log
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import com.facebook.stetho.Stetho
import com.sociograph.davechatbot.base.showDialog
import com.sociograph.davechatbot.core.ConfigData
import com.sociograph.davechatbot.core.Injection
import com.sociograph.davechatbot.domain.support_domain.models.AppException
import com.sociograph.davechatbot.domain.support_domain.schedulers.Scheduler
import com.sociograph.davechatbot.domain.usecase.SendMessageUseCase
import com.sociograph.davechatbot.ui.chat.ChatViewActivity
import com.sociograph.davechatbot.ui.feedback.FeedbackViewDialog
import io.reactivex.plugins.RxJavaPlugins

class DaveChatBotSdk {


    companion object {
        private val TAG = DaveChatBotSdk::class.simpleName

        private var INSTANCE: DaveChatBotSdk? = null
        lateinit var appContext: Context

        fun initialize(
            context: Context,
            signupApiKey: String,
            enterpriseId: String,
            conversationId: String
        ): DaveChatBotSdk {
            Log.d(TAG, "DaveChatBoatSdk Initialize")
            if (INSTANCE == null) {
                appContext = context
                ConfigData.SIGNUP_API_KEY = signupApiKey
                ConfigData.CONVERSATION_ID = conversationId
                ConfigData.ENTERPRISE_ID = enterpriseId
                synchronized(DaveChatBotSdk::javaClass) {
                    INSTANCE = DaveChatBotSdk()
                }
            }
            return INSTANCE!!
        }

        fun getInstance(): DaveChatBotSdk {
            if (INSTANCE != null) {
                return INSTANCE!!
            } else {
                throw IllegalAccessException("Please call DaveChatBotSdk.initialize() before accessing instance.")
            }
        }

        fun login(
            userName: String, password: String,
            onSuccess: () -> Unit,
            failureMessage: (message: String) -> Unit,
            onError: (exception: Throwable) -> Unit
        ) {
            INSTANCE?.callLogin(
                userName,
                password,
                onSuccess,
                failureMessage,
                onError
            )
        }

        fun sendMessage(
            customerState: String,
            customerResponse: String,
            onSuccess: () -> Unit,
            failureMessage: (message: String) -> Unit,
            onError: (exception: Throwable) -> Unit
        ) {
            INSTANCE?.callConversation(
                customerResponse,
                customerState,
                onSuccess,
                failureMessage,
                onError
            )
        }

        fun submitFeedback(fragmentManager: FragmentManager) {
            INSTANCE?.submitFeedback(fragmentManager)
        }

        fun startChatBotConversation(context: Context) {
            if (isLogin()) {
                INSTANCE?.startChatBotConversation(context)
            } else {
                Toast.makeText(context, "Please login first", Toast.LENGTH_SHORT).show()
            }
        }

        fun logout() {
            INSTANCE?.callLogout()
        }

        fun isLogin(): Boolean {
            return INSTANCE?.isLogin() ?: false
        }
    }


    private val appSettingsDataSource = Injection.provideAppSettingDataSource()
    private val appDataSource = Injection.provideAppDataSource()
    private val uiSettings = appSettingsDataSource.uiSetting

    init {

        initDebuggers()
        RxJavaPlugins.setErrorHandler {
        }

        buildAndSetDefaultUISettings()
        Log.w(TAG, "Initialize DaveChatBotSDK Version: " + BuildConfig.VERSION_CODE)
    }

    private fun initDebuggers() {
        if (BuildConfig.DEBUG) {
            Stetho.initialize(
                Stetho.newInitializerBuilder(appContext)
                    .enableDumpapp(Stetho.defaultDumperPluginsProvider(appContext))
                    .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(appContext))
                    .build()
            )
            Stetho.initializeWithDefaults(appContext)
        }
    }

    private fun callLogin(
        userName: String,
        password: String,
        onSuccess: () -> Unit,
        failureMessage: (message: String) -> Unit,
        onError: (exception: Throwable) -> Unit
    ) {
        callLogout()
        val subscribe = appDataSource.login(
            userName,
            password,
            ConfigData.USER_MODEL_ROLE,
            ConfigData.LOGIN_ATTRS,
            ConfigData.ENTERPRISE_ID
        ).subscribeOn(Scheduler.io())
            .observeOn(Scheduler.ui()).subscribe({
                appSettingsDataSource.loginDetail = it
                onSuccess()
            }, {
                if (it is AppException && it.error != null) {
                    failureMessage(it.error!!.message)
                } else {
                    onError(it)
                }

            })
    }

    private fun callConversation(
        customerState: String,
        customerResponse: String,
        onSuccess: () -> Unit,
        failureMessage: (message: String) -> Unit,
        onError: (exception: Throwable) -> Unit
    ) {
        if (appSettingsDataSource.loginDetail == null) {
            failureMessage("Please login first")
            return
        }
        val params = SendMessageUseCase.Params(
            ConfigData.CONVERSATION_ID,
            customerResponse,
            customerState,
            "auto"
        )
        val subscribe =
            Injection.provideSendMessageUseCase().execute(params).subscribeOn(Scheduler.io())
                .observeOn(Scheduler.ui()).subscribe({
                    onSuccess()
                    startChatActivity()
                }, {
                    if (it is AppException && it.error != null) {
                        failureMessage(it.error!!.message)
                    } else {
                        onError(it)
                    }
                })
    }

    private fun startChatActivity() {
        val intent = Intent(appContext, ChatViewActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        appContext.startActivity(intent)
    }

    private fun callLogout() {
        appSettingsDataSource.clearLocalStorage()
        appDataSource.clearDatabase()
    }

    private fun isLogin(): Boolean {
        return appSettingsDataSource.isLogin
    }

    private fun buildAndSetDefaultUISettings() {
        uiSettings.buttonColor = ContextCompat.getColor(appContext, R.color.white)
        uiSettings.buttonTextColor = ContextCompat.getColor(appContext, R.color.black)
        uiSettings.backgroundViewColor = ContextCompat.getColor(appContext, R.color.white)
        uiSettings.chatBubbleColor = ContextCompat.getColor(appContext, R.color.colorSecondary)
        uiSettings.chatBubbleTextColor = ContextCompat.getColor(appContext, R.color.black)
        uiSettings.headerColor = ContextCompat.getColor(appContext, R.color.colorPrimary)
        uiSettings.sendIcon = ContextCompat.getDrawable(appContext, R.drawable.ic_send)
        uiSettings.closeIcon = ContextCompat.getDrawable(appContext, R.drawable.ic_close)
        uiSettings.loaderImage = ContextCompat.getDrawable(appContext, R.drawable.ic_chat)
        uiSettings.chatBotIcon = ContextCompat.getDrawable(appContext, R.drawable.ic_dave_icon)
        uiSettings.chatUserIcon =
            ContextCompat.getDrawable(appContext, R.drawable.ic_user_chat_icon)
        uiSettings.optionsText = appContext.getString(R.string.lbl_this_is_option)
        uiSettings.titleText = appContext.getString(R.string.lbl_chatbot)

    }

    fun setButtonColorResource(@ColorRes resourceId: Int) {
        setButtonColor(ContextCompat.getColor(appContext, resourceId))
    }


    fun setButtonColor(color: Int) {
        uiSettings.buttonColor = color
    }

    fun setButtonTextColorResource(@ColorRes resourceId: Int) {
        setButtonTextColor(ContextCompat.getColor(appContext, resourceId))
    }


    fun setButtonTextColor(color: Int) {
        uiSettings.buttonTextColor = color
    }

    fun setBackgroundViewColorResource(@ColorRes resourceId: Int) {
        setBackgroundViewColor(ContextCompat.getColor(appContext, resourceId))
    }


    fun setBackgroundViewColor(color: Int) {
        uiSettings.backgroundViewColor = color
    }

    fun setChatBubbleColorResource(@ColorRes resourceId: Int) {
        setChatBubbleColor(ContextCompat.getColor(appContext, resourceId))
    }


    fun setChatBubbleColor(color: Int) {
        uiSettings.chatBubbleColor = color
    }

    fun setChatBubbleTextColorResource(@ColorRes resourceId: Int) {
        setChatBubbleTextColor(ContextCompat.getColor(appContext, resourceId))
    }


    fun setChatBubbleTextColor(color: Int) {
        uiSettings.chatBubbleTextColor = color
    }

    fun setHeaderColorResource(@ColorRes resourceId: Int) {
        setHeaderColor(ContextCompat.getColor(appContext, resourceId))
    }


    fun setHeaderColor(color: Int) {
        uiSettings.headerColor = color
    }


    fun setSendIconResource(@DrawableRes resourceId: Int) {
        setSendIcon(ContextCompat.getDrawable(appContext, resourceId))
    }

    fun setSendIcon(drawable: Drawable?) {
        uiSettings.sendIcon = drawable
    }

    fun setCloseIconResource(@DrawableRes resourceId: Int) {
        setCloseIcon(ContextCompat.getDrawable(appContext, resourceId))
    }

    fun setCloseIcon(drawable: Drawable?) {
        uiSettings.closeIcon = drawable
    }


    fun setLoaderImageResource(@DrawableRes resourceId: Int) {
        setLoaderImage(ContextCompat.getDrawable(appContext, resourceId))
    }

    fun setLoaderImage(drawable: Drawable?) {
        uiSettings.loaderImage = drawable
    }

    fun setChatBoatIconResource(@DrawableRes resourceId: Int) {
        setChatBoatIcon(ContextCompat.getDrawable(appContext, resourceId))
    }

    fun setChatBoatIcon(drawable: Drawable?) {
        uiSettings.chatBotIcon = drawable
    }

    fun setChatUserIconResource(@DrawableRes resourceId: Int) {
        setChatUserIcon(ContextCompat.getDrawable(appContext, resourceId))
    }

    fun setChatUserIcon(drawable: Drawable?) {
        uiSettings.chatUserIcon = drawable
    }

    fun setOptionsTextResource(@StringRes resourceId: Int) {
        setOptionsText(appContext.getString(resourceId))
    }

    fun setOptionsText(value: String) {
        uiSettings.optionsText = value
    }

    fun setTitleTextResource(@StringRes resourceId: Int) {
        setTitleText(appContext.getString(resourceId))
    }

    fun setTitleText(value: String) {
        uiSettings.titleText = value
    }

    private fun submitFeedback(fragmentManager: FragmentManager) {
        if (appSettingsDataSource.engagementId != null) {
            FeedbackViewDialog().showDialog(fragmentManager)
        } else {
            FeedbackViewDialog().showDialog(fragmentManager)
            //throw IllegalAccessException("There is not interaction done between user and ChatBot")
        }
    }

    private fun startChatBotConversation(context: Context) {
        ChatViewActivity.startActivity(context)
    }
}

/*      val daveChatBotSdk = DaveChatBotSdk.getInstance()
        daveChatBotSdk.setButtonColorResource(R.color.white)
        daveChatBotSdk.setButtonTextColorResource(R.color.black)
        daveChatBotSdk.setBackgroundViewColorResource(R.color.white)
        daveChatBotSdk.setChatBubbleColorResource(R.color.colorSecondary)
        daveChatBotSdk.setChatBubbleTextColorResource(R.color.black)
        daveChatBotSdk.setHeaderColorResource(R.color.colorPrimary)
        daveChatBotSdk.setSendIconResource(R.drawable.ic_send)
        daveChatBotSdk.setCloseIconResource(R.drawable.ic_close)
        daveChatBotSdk.setLoaderImageResource(R.drawable.ic_chat)
        daveChatBotSdk.setChatBoatIconResource(R.drawable.ic_dave_icon)
        daveChatBotSdk.setChatUserIconResource(R.drawable.ic_user_chat_icon)
        daveChatBotSdk.setOptionsTextResource(R.string.lbl_this_is_option)
        daveChatBotSdk.setTitleTextResource(R.string.lbl_chatbot)*/