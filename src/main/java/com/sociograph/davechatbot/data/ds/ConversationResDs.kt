package com.sociograph.davechatbot.data.ds


import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import com.sociograph.davechatbot.data.webservices.GsonUtils
import com.sociograph.davechatbot.data.webservices.JsonUtils
import com.sociograph.davechatbot.domain.resmodels.*
import java.lang.reflect.Type

internal class ConversationResDs : JsonDeserializer<ConversationRes> {

    @Throws(JsonParseException::class)
    override fun deserialize(
        json: JsonElement,
        typeOfT: Type,
        context: JsonDeserializationContext
    ): ConversationRes {
        val gson = GsonUtils.getInstance().gson

        val jsonObject = json.asJsonObject

        val conversationRes = ConversationRes()

        if (JsonUtils.hasProperty(jsonObject, "customer_state")) {
            conversationRes.customerState = jsonObject.get("customer_state").asString
        }


        if (JsonUtils.hasProperty(jsonObject, "data")) {
            val dataJson = jsonObject.getAsJsonObject("data")
            conversationRes.data = gson.fromJson(dataJson, ConversationData::class.java)
        }

        if (JsonUtils.hasProperty(jsonObject, "engagement_id")) {
            conversationRes.engagementId = jsonObject.get("engagement_id").asString
        }


        if (JsonUtils.hasProperty(jsonObject, "maintain_whiteboard")) {
            conversationRes.maintainWhiteboard = jsonObject.get("maintain_whiteboard").asBoolean
        }

        if (JsonUtils.hasProperty(jsonObject, "name")) {
            conversationRes.name = jsonObject.get("name").asString
        }

        if (JsonUtils.hasProperty(jsonObject, "options")) {
            conversationRes.options = jsonObject.get("options")
        }

        if (JsonUtils.hasProperty(jsonObject, "overwrite_whiteboard")) {
            conversationRes.overwriteWhiteboard = jsonObject.get("overwrite_whiteboard").asBoolean
        }


        if (JsonUtils.hasProperty(jsonObject, "placeholder")) {
            conversationRes.placeholder = jsonObject.get("placeholder").asString
        }

        if (JsonUtils.hasProperty(jsonObject, "placeholder_aliases")) {
            val placeholderAliasesJson = jsonObject.getAsJsonObject("placeholder_aliases")
            conversationRes.placeholderAliases =
                gson.fromJson(placeholderAliasesJson, PlaceholderAliases::class.java)
        }

        if (JsonUtils.hasProperty(jsonObject, "response_channels")) {
            val responseChannelsJson = jsonObject.getAsJsonObject("response_channels")
            conversationRes.responseChannels =
                gson.fromJson(responseChannelsJson, ResponseChannels::class.java)
        }

        if (JsonUtils.hasProperty(jsonObject, "response_id")) {
            conversationRes.responseId = jsonObject.get("response_id").asString
        }

        if (JsonUtils.hasProperty(jsonObject, "show_feedback")) {
            conversationRes.showFeedback = jsonObject.get("show_feedback").asBoolean
        }

        if (JsonUtils.hasProperty(jsonObject, "show_in_history")) {
            conversationRes.showInHistory = jsonObject.get("show_in_history").asBoolean
        }

        if (JsonUtils.hasProperty(jsonObject, "state_options")) {
            jsonObject.get("state_options").asJsonObject.entrySet().forEach { stateOptionJson ->
                val key = stateOptionJson.key
                val value = stateOptionJson.value.asString
                conversationRes.stateOptions.add(StateOptions(key, value))
            }
        }


        if (JsonUtils.hasProperty(jsonObject, "title")) {
            conversationRes.title = jsonObject.get("title").asString
        }

        if (JsonUtils.hasProperty(jsonObject, "to_state_function")) {
            val toStateFunctionJson = jsonObject.getAsJsonObject("to_state_function")
            conversationRes.toStateFunction =
                gson.fromJson(toStateFunctionJson, ToStateFunction::class.java)
        }

        if (JsonUtils.hasProperty(jsonObject, "wait")) {
            conversationRes.wait = jsonObject.get("wait").asInt
        }

        if (JsonUtils.hasProperty(jsonObject, "whiteboard")) {
            conversationRes.whiteboard = jsonObject.get("whiteboard").asString
        }

        if (JsonUtils.hasProperty(jsonObject, "whiteboard_title")) {
            conversationRes.whiteboardTitle = jsonObject.get("whiteboard_title").asString
        }

        if (JsonUtils.hasProperty(jsonObject, "timestamp")) {
            conversationRes.timestamp = jsonObject.get("timestamp").asString
        }

        if (JsonUtils.hasProperty(jsonObject, "direction")) {
            conversationRes.direction = jsonObject.get("direction").asString
        }

        if (JsonUtils.hasProperty(jsonObject, "sequence")) {
            conversationRes.sequence = jsonObject.get("sequence").asInt
        }
        return conversationRes
    }
}
