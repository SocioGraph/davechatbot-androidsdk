package com.sociograph.davechatbot.data.ds


import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import com.google.gson.reflect.TypeToken
import com.sociograph.davechatbot.data.webservices.GsonUtils
import com.sociograph.davechatbot.data.webservices.JsonUtils
import com.sociograph.davechatbot.domain.models.KeywordItem
import com.sociograph.davechatbot.domain.resmodels.ConversationKeywordRes
import java.lang.reflect.Type

internal class ConversationKeywordResDs : JsonDeserializer<ConversationKeywordRes> {

    @Throws(JsonParseException::class)
    override fun deserialize(
        json: JsonElement,
        typeOfT: Type,
        context: JsonDeserializationContext
    ): ConversationKeywordRes {
        val gson = GsonUtils.getInstance().gson

        val jsonObject = json.asJsonObject

        val conversationKeywordRes = ConversationKeywordRes()

        if (JsonUtils.hasProperty(jsonObject, "keywords")) {
            val keywordArray = jsonObject.get("keywords").asJsonArray

            keywordArray.forEach {
                val itemArray = it.asJsonArray
                val keywordItem = KeywordItem()
                itemArray.forEachIndexed { index, jsonElement ->
                    when (index) {
                        0 -> {
                            keywordItem.mixedList = jsonElement.asString
                        }
                        1 -> {
                            keywordItem.customerState = jsonElement.asString
                        }
                        2 -> {
                            keywordItem.customerResponse = jsonElement.asString
                        }
                        5 -> {
                            val type = object : TypeToken<ArrayList<String>>() {}.type
                            keywordItem.keywordList =
                                gson.fromJson(
                                    jsonElement, type
                                )
                        }
                    }
                }
                conversationKeywordRes.keywords.add(keywordItem)
            }


        }
        return conversationKeywordRes
    }
}
