package com.sociograph.davechatbot.data.ds


import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import com.sociograph.davechatbot.data.webservices.JsonUtils
import com.sociograph.davechatbot.domain.support_domain.models.MError
import java.lang.reflect.Type

/**
 */

internal class ErrorDs : JsonDeserializer<MError> {
    /*{
        "Code":"1005",
            "Name":"VerificationFailed",
            "Message":
        "We're sorry but we are unable to verify your account. Please contact our Customer Relations department so they can assist you at 1-800-432-6111"
    }*/

    @Throws(JsonParseException::class)
    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): MError {
        val error = MError()
        val jsonObject = json.asJsonObject
        if (JsonUtils.hasProperty(jsonObject, "Code")) {
            val code = jsonObject.get("Code").asString
            error.code = code
        }
        if (JsonUtils.hasProperty(jsonObject, "Name")) {
            val name = jsonObject.get("Name").asString
            error.name = name
        }
        if (JsonUtils.hasProperty(jsonObject, "Message")) {
            val message = jsonObject.get("Message").asString
            error.message = message
        }
        return error
    }
}
