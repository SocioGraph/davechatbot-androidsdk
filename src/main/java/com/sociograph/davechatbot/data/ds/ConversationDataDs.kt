package com.sociograph.davechatbot.data.ds


import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import com.google.gson.reflect.TypeToken
import com.sociograph.davechatbot.data.webservices.GsonUtils
import com.sociograph.davechatbot.data.webservices.JsonUtils
import com.sociograph.davechatbot.domain.resmodels.ConversationData
import com.sociograph.davechatbot.domain.resmodels.FormItem
import com.sociograph.davechatbot.domain.resmodels.Options
import com.sociograph.davechatbot.domain.resmodels.ThumbImageItem
import java.lang.reflect.Type

internal class ConversationDataDs : JsonDeserializer<ConversationData> {

    @Throws(JsonParseException::class)
    override fun deserialize(
        json: JsonElement,
        typeOfT: Type,
        context: JsonDeserializationContext
    ): ConversationData {
        val gson = GsonUtils.getInstance().gson

        val jsonObject = json.asJsonObject

        val conversationData = ConversationData()

        if (JsonUtils.hasProperty(jsonObject, "response_type")) {
            conversationData.responseType = jsonObject.get("response_type").asString
        }

        if (JsonUtils.hasProperty(jsonObject, "customer_state")) {
            conversationData.customerState = jsonObject.get("customer_state").asString
        }

        if (JsonUtils.hasProperty(jsonObject, "options")) {
            val optionsJson = jsonObject.get("options")
            /*val type = object : TypeToken<ArrayList<Any>>() {}.type
            conversationData.options = gson.fromJson(optionsJson, type)*/

            /*if (optionsJson.isJsonArray) {
                val type = object : TypeToken<ArrayList<Any>>() {}.type
                conversationData.options = gson.fromJson(optionsJson, type)
            } else if (optionsJson.isJsonObject) {
                val optionList = arrayListOf<Any>()
                optionsJson.asJsonObject.entrySet().forEach {
                    optionList.add(arrayListOf(it.key, it.value))
                }
                conversationData.options = optionList
            } else {
                conversationData.options = arrayListOf()
            }*/

            try {
                if (optionsJson.isJsonArray) {
                    val optionList = arrayListOf<Options>()
                    optionsJson.asJsonArray.forEach {
                        if (it.isJsonArray) {
                            optionList.add(
                                Options(
                                    it.asJsonArray[0].asString,
                                    it.asJsonArray[1].asString
                                )
                            )
                        } else if (it.isJsonObject) {
                            it.asJsonObject.entrySet().forEach {
                                optionList.add(Options(it.key, it.value.asString))
                            }
                        } else if (it.isJsonPrimitive) {
                            optionList.add(Options("", it.asString))
                        }
                    }
                    conversationData.options = optionList
                } else if (optionsJson.isJsonObject) {
                    val optionList = arrayListOf<Options>()
                    optionsJson.asJsonObject.entrySet().forEach {
                        optionList.add(Options(it.key, it.value.asString))
                    }
                    conversationData.options = optionList
                } else {
                    conversationData.options = arrayListOf()
                }
            } catch (e: Exception) {
                e.printStackTrace()
                conversationData.options = arrayListOf()
            }

        }

        if (JsonUtils.hasProperty(jsonObject, "image")) {
            conversationData.image = jsonObject.get("image").asString
        }

        if (JsonUtils.hasProperty(jsonObject, "url")) {
            conversationData.url = jsonObject.get("url").asString
        }

        if (JsonUtils.hasProperty(jsonObject, "video")) {
            conversationData.video = jsonObject.get("video").asString
        }

        if (JsonUtils.hasProperty(jsonObject, "form")) {
            val formJson = jsonObject.get("form")
            val type = object : TypeToken<ArrayList<FormItem>>() {}.type
            conversationData.form = gson.fromJson(formJson, type)
        }

        if (JsonUtils.hasProperty(jsonObject, "_follow_ups")) {
            val formJson = jsonObject.get("_follow_ups")
            val type = object : TypeToken<List<String>>() {}.type
            conversationData.followUps = gson.fromJson(formJson, type)
        }

        if (JsonUtils.hasProperty(jsonObject, "thumbnails")) {
            val thumbnailsJson = jsonObject.get("thumbnails")
            val type = object : TypeToken<ArrayList<ThumbImageItem>>() {}.type
            conversationData.thumbnails = gson.fromJson(thumbnailsJson, type)
        }

        return conversationData
    }
}
