package com.sociograph.davechatbot.data.webservices

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.sociograph.davechatbot.data.ds.*
import com.sociograph.davechatbot.domain.resmodels.ConversationData
import com.sociograph.davechatbot.domain.resmodels.ConversationHistoryRes
import com.sociograph.davechatbot.domain.resmodels.ConversationKeywordRes
import com.sociograph.davechatbot.domain.resmodels.ConversationRes
import com.sociograph.davechatbot.domain.support_domain.models.MError


internal class GsonUtils private constructor() {
    public var gson: Gson

    init {
        val gsonBuilder = GsonBuilder().setLenient()
        registerTypeAdapters(gsonBuilder)
        gson = gsonBuilder.create()
    }

    private fun registerTypeAdapters(gsonBuilder: GsonBuilder) {
        addSerializers(gsonBuilder)

        gsonBuilder.registerTypeAdapter(MError::class.java, ErrorDs())
        gsonBuilder.registerTypeAdapter(ConversationRes::class.java, ConversationResDs())
        gsonBuilder.registerTypeAdapter(ConversationData::class.java, ConversationDataDs())
        gsonBuilder.registerTypeAdapter(
            ConversationHistoryRes::class.java,
            ConversationHistoryResDs()
        )
        gsonBuilder.registerTypeAdapter(
            ConversationKeywordRes::class.java,
            ConversationKeywordResDs()
        )
    }

    private fun addSerializers(gsonBuilder: GsonBuilder) {
    }

    companion object {
        private var instance: GsonUtils? = null

        fun getInstance(): GsonUtils {
            if (instance == null) {
                instance =
                        GsonUtils()
            }
            return instance as GsonUtils
        }
    }
}
