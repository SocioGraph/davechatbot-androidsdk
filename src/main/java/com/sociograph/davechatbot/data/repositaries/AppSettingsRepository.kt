package com.sociograph.davechatbot.data.repositaries

import android.content.Context
import androidx.annotation.VisibleForTesting
import com.google.gson.Gson
import com.sociograph.davechatbot.UISetting
import com.sociograph.davechatbot.data.support_data.shared_pref.BaseLiveSharedPreferences
import com.sociograph.davechatbot.data.support_data.shared_pref.Prefs
import com.sociograph.davechatbot.domain.datasources.IAppSettingsDataSource
import com.sociograph.davechatbot.domain.resmodels.LoginRes


internal class AppSettingsRepository(
    private val applicationContext: Context,
    private val plainGson: Gson
) :
    IAppSettingsDataSource {


    private var liveSharedPreferences: BaseLiveSharedPreferences

    private val SP_NAME = "prefs_chat_bot_sdk"

    private var _systemResponse: String? = null
    private var _isLoadedFirstTime: Boolean = false
    private var _followUps: String = ""
    private var _waitingTime: Int = -1
    private var _uiSetting: UISetting = UISetting()

    companion object {

        private var INSTANCE: AppSettingsRepository? = null

        @JvmStatic
        fun getInstance(applicationContext: Context, plainGson: Gson): AppSettingsRepository {
            if (INSTANCE == null) {
                synchronized(AppSettingsRepository::javaClass) {
                    INSTANCE = AppSettingsRepository(applicationContext, plainGson)
                }
            }
            return INSTANCE!!
        }

        @VisibleForTesting
        fun clearInstance() {
            INSTANCE = null
        }

        private const val PREF_USER_ID = "PREF_USER_ID"
        private const val PREFS_AUTH_TOKEN = "PREF_AUTH_TOKEN"
        private const val PREF_UNIQUE_ID = "PREF_UNIQUE_ID"
        private const val PREF_LOGIN_DETAIL = "PREF_LOGIN_DETAIL"
        private const val PREF_ENGAGEMENT_ID = "PREF_ENGAGEMENT_ID"
        private const val PREF_PAGE_NUMBER = "PREF_PAGE_NUMBER"


    }

    init {
        Prefs.Builder()
            .setContext(applicationContext)
            .setMode(Context.MODE_PRIVATE)
            .setPrefsName(SP_NAME)
            .build()

        liveSharedPreferences = BaseLiveSharedPreferences(Prefs.getPreferences())

    }

    override var loginDetail: LoginRes?
        get() {
            val loginResStr = Prefs.getString(PREF_LOGIN_DETAIL, null)
            loginResStr?.let {
                return plainGson.fromJson(loginResStr, LoginRes::class.java)
            }
            return null
        }
        set(value) {
            val loginResStr = plainGson.toJson(value)
            userId = value?.userId ?: ""
            Prefs.putString(PREF_LOGIN_DETAIL, loginResStr)
        }

    override var userId: String
        get() = Prefs.getString(PREF_USER_ID, "")
        set(value) {
            Prefs.putString(PREF_USER_ID, value)
        }

    override val isLogin: Boolean
        get() = Prefs.contains(PREF_USER_ID)

    override var engagementId: String?
        get() = Prefs.getString(PREF_ENGAGEMENT_ID, "")
        set(value) {
            Prefs.putString(PREF_ENGAGEMENT_ID, value)
        }

    override var systemResponse: String?
        get() = _systemResponse
        set(value) {
            _systemResponse = value
        }

    override fun clearLocalStorage() {
        Prefs.clear()
        _systemResponse = null
    }

    override var isLoadedFirstTime: Boolean
        get() = _isLoadedFirstTime
        set(value) {
            _isLoadedFirstTime = value
        }

    override var followUps: String
        get() = _followUps
        set(value) {
            _followUps = value
        }

    override var waitingTime: Int
        get() = _waitingTime
        set(value) {
            _waitingTime = value
        }

    override val uiSetting: UISetting
        get() = _uiSetting

    override var pageNumber: Int
        get() = Prefs.getInt(PREF_PAGE_NUMBER, 1)
        set(value) {
            Prefs.putInt(PREF_PAGE_NUMBER, value)
        }
}